// classList - shows/gets all classes
// contains - checks classList for specific class
// add - add class
// remove - remove class
// toggle - toggles class

const navToggle = document.querySelector('.nav-toggle');
const links = document.querySelector('.nav-center');

navToggle.addEventListener('click', function (){
  links.classList.toggle('show-links');
});


// Burger-menu

(function() {
  var burger2;

  burger2 = document.querySelector(".burger2");

  burger2.addEventListener("click", function() {
    return burger2.classList.toggle("on");
  });

}).call(this);


// Account connect link

const connectlink = document.querySelector('.connect-link');
const dropdownMenu = document.querySelector('.dropdown-menu');

connectlink.addEventListener('click', function (){
  dropdownMenu.classList.toggle('show-dropdown-menu');
});